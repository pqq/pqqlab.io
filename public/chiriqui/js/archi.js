// 240809 // klevstul
// ----
// set title
// ----
let version = '.240815';
document.title += ' v' + version;
// ----
// toggle an element visible or hidden
// ----
function toggleDisplay(element_id) {
    var element = document.getElementById(element_id);
    if (element) {
        if (element.style.display === "none") {
            element.style.display = "block";
        }
        else {
            element.style.display = "none";
        }
    }
}
// ----
// toggle both a button and an element visible or hidden
// ----
function toggleElementAndButton(element_name) {
    let storage_id = 'chiriqui.' + element_name;
    let element_id = storage_id;
    let button_on_id = 'chiriqui.btn.' + element_name + '.on';
    let button_off_id = 'chiriqui.btn.' + element_name + '.off';
    let storage_value = sessionStorage.getItem(storage_id);
    let storage_value_int = 0;
    if (storage_value == null) {
        storage_value_int = 1;
    }
    else {
        storage_value_int = parseInt(storage_value);
    }
    if (storage_value_int == 0) {
        console.log('set ' + element_name + ' OFF');
        toggleDisplay(button_off_id);
        toggleDisplay(element_id);
    }
    else {
        console.log('set ' + element_name + ' ON');
        toggleDisplay(button_on_id);
    }
}
// ----
// reset values
// ----
let reset_values = sessionStorage.getItem('chiriqui.reset_values');
let reset_values_int = 0;
if (reset_values !== null) {
    reset_values_int = parseInt(reset_values);
}
if (reset_values_int == 1) {
    console.log('resetting values');
    sessionStorage.clear();
}
// ---
// set defaults
// ---
if (sessionStorage.getItem('chiriqui.news.world') == null) {
    sessionStorage.setItem('chiriqui.news.world', '0');
}
// ----
// auto refresh
// ----
let reload_timer = sessionStorage.getItem('chiriqui.reload_timer');
let reload_timer_int = 0;
if (reload_timer !== null) {
    reload_timer_int = parseInt(reload_timer);
}
if (reload_timer_int == 0) {
    console.log('auto refresh OFF');
    toggleDisplay('chiriqui.btn.autorefresh.off');
    setTimeout(function () { }, 0);
}
else {
    console.log('auto refresh ON');
    toggleDisplay('chiriqui.btn.autorefresh.on');
    setTimeout(function () { window.location = window.location; }, reload_timer_int);
}
// ------
// world news
// ------
let output_element = document.getElementById("chiriqui.news.world.title");
if (output_element) {
    output_element.innerHTML = "<h1>world news about panama and chiriqui</h1>";
}
toggleElementAndButton('news.world');
// ------
// local msm news
// ------
output_element = document.getElementById("chiriqui.news.msm.title");
if (output_element) {
    output_element.innerHTML = "<h1>local mainstream news</h1>";
}
toggleElementAndButton('news.msm');
// ------
// local alt news
// ------
output_element = document.getElementById("chiriqui.news.alt.title");
if (output_element) {
    output_element.innerHTML = "<h1>local alternative news</h1>";
}
toggleElementAndButton('news.alt');
// ------
// webSearch
// ------
output_element = document.getElementById("chiriqui.webSearch.title");
if (output_element) {
    output_element.innerHTML = "<h1>web search</h1>";
}
toggleElementAndButton('webSearch');
// ------
// some
// ------
output_element = document.getElementById("chiriqui.some.title");
if (output_element) {
    output_element.innerHTML = "<h1>social media</h1>";
}
toggleElementAndButton('some');
// ------
// instagram
// ------
output_element = document.getElementById("chiriqui.instagram.title");
if (output_element) {
    output_element.innerHTML = "<h1>instagram</h1>";
}
toggleElementAndButton('instagram');
// ------
// windy
// ------
output_element = document.getElementById("chiriqui.windy.title");
if (output_element) {
    output_element.innerHTML = "<h1>weather</h1>";
}
toggleElementAndButton('windy');
// ------
// earthquakes
// ------
output_element = document.getElementById("chiriqui.earthquakes.title");
if (output_element) {
    output_element.innerHTML = "<h1>earthquakes</h1>";
}
toggleElementAndButton('earthquakes');
