// 240809 // klevstul
function view_toggle(element_id) {
    var element = document.getElementById(element_id);
    if (element) {
        if (element.style.display === "none") {
            element.style.display = "block";
        }
        else {
            element.style.display = "none";
        }
    }
}
